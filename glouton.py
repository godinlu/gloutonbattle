from farm import Farm
from pioche import Pioche
from aliment import Aliment
from recette import Recette
from farm import Farm
from carteData import (cartes_recette,nb_cartes_recette,cartes_farm,nb_cartes_farm,cartes_aliment,nb_cartes_aliment)

class Glouton:
    def __init__(self,a_name):
        self.name = a_name
        self.PV = 30 #désigne les point de vie de ce glouton, défaut:20
        self.attaque = 0 #désigne l'attaque de ce glouton, défaut:0
        self.regenerationPV = 0 #désigne le nb de point de vie que régénère ce glouton par tour, défaut:0
        self.regenerationCalories = 3 #désigne le nb de calories que ce glouton gagne par tour, défaut:3
        self.nbCarteParTour = 1 #désigne le nombre de carte que ce glouton pioche par tour, défaut:1
        self.calories = 3 #calories désigne les calories que ce glouton possède
        self.main = [] #main désigne les cartes que ce glouton possède, max:8
        self.ingredient = [1,1,1,1] #désigne les ingrédients que possède ce glouton de la forme [nbViande,nbPatates,nbFromages,nbPates]
        self.farm = [0,0,0,0] #désigne les niveau de farm que possède ce glouton de la forme [lvl_Farm_Viande,lvl_farm_Patates,lvl_farm_Fromages,lvl_farm_Pates]




    def initialisationPioche(self):
        self.pioche = Pioche()
        #créer la pioche des cartes farm et ajoute les bons nombre de carte dans la pioche en fonction de nb_cartes_farm
        i=0
        for nbCarte in nb_cartes_farm:
            for x in range(nbCarte):
                self.pioche.addCartes(cartes_farm[i])
            i=i+1

        #créer la pioche des cartes recettes et ajoute les bons nombre de carte dans la pioche en fonction de nb_cartes_farm
        i=0
        for nbCarte in nb_cartes_recette:
            for x in range(nbCarte):
                self.pioche.addCartes(cartes_recette[i])
            i=i+1
        #créer la pioche des cartes recettes et ajoute les bons nombre de carte dans la pioche en fonction de nb_cartes_farm
        i=0
        for nbCarte in nb_cartes_aliment:
            for x in range(nbCarte):
                self.pioche.addCartes(cartes_aliment[i])
            i=i+1

        self.pioche.shuffle()

    #pioche les cartes de départ pour ce glouton
    def piocheDesCartesInitial(self):
        self.piocheCarteAliment()
        self.piocheCarteFarm()
        self.piocheCarteRecette()
        self.piocheCarteRecette()

    def piocheCarteRecette(self):
        self.main.append(self.pioche.piocheCarteRecette())

    def piocheCarteFarm(self):
        self.main.append(self.pioche.piocheCarteFarme())

    def piocheCarteAliment(self):
        self.main.append(self.pioche.piocheCarteAliment())

#peutPoserCarte renvoie si oui le glouton a les ressources necessaire de poser la carte si il peut alors 0 est renvoyé si il ne peut pas
#1 ou 2 est renvoyé avec 1 pour un manque de calories et 2 pour un manque de nourriture si la carte est une recette
    def peutPoserCarte(self,idCarte)-> int:
        carte = self.main[idCarte]
        if isinstance(carte,Aliment):
            return 0
        else:
            #teste si la carte côute plus de calories que le glouton en a en reserve et renvoie le message approrpié
            if carte.coutCalorie > self.calories:
                return 1
            else:
                if isinstance(carte,Farm):
                    return 0
                else:
                    #teste si le glouton possède tous les ingrédients necessaire pour faire cette recette si oui alors renvoie 0 sinon renvoie 2
                    if carte.ingredients[0] > self.ingredient[0] or carte.ingredients[1] > self.ingredient[1] or carte.ingredients[2] > self.ingredient[2] or carte.ingredients[3] > self.ingredient[3]:
                        return 2
                    else:
                        return 0

#précondition, la carte en question doit d'abord être passé par la fonction peutPoserCarte
#la fonction joueCarte active le pouvoir de la carte qui a comme id dans la main idCarte
    def joueCarte(self,idCarte) -> int:
        carte = self.main[idCarte]
        self.main.pop(idCarte)
        #si la carte est un aliment alors les ingrédients de la carte doivent être ajouté au ingrédients du glouton
        if isinstance(carte,Aliment):
            self.calories += carte.apportCalories
            self.ingredient[0] += carte.apportIngredient[0]
            self.ingredient[1] += carte.apportIngredient[1]
            self.ingredient[2] += carte.apportIngredient[2]
            self.ingredient[3] += carte.apportIngredient[3]
        #si la carte est une farm alors
        elif isinstance(carte,Farm):
            self.calories = self.calories - carte.coutCalorie #les calories du glouton sont mise a jour en fonction du côut en calories de la carte
            #les farm sont mis à niveau en fonction du type de farm de la carte
            self.farm[0] += carte.revenue[0]
            self.farm[1] += carte.revenue[1]
            self.farm[2] += carte.revenue[2]
            self.farm[3] += carte.revenue[3]
        #si la carte est un recette alors on enlève les ingrédients et les calories au glouton et fait apelle a la fonction mangeRecette
        elif isinstance(carte,Recette):
            self.calories = self.calories - carte.coutCalorie
            self.ingredient[0] -= carte.ingredients[0]
            self.ingredient[1] -= carte.ingredients[1]
            self.ingredient[2] -= carte.ingredients[2]
            self.ingredient[3] -= carte.ingredients[3]

            return self.mangeRecette(carte)


#la fonction mangeRecette prend en paramètre une carte recette et augmente les stats en fonction de la recette
    def mangeRecette(self,recette) -> int:
        #gain durable
        self.attaque += recette.gainDurable[0]
        self.regenerationPV += recette.gainDurable[1]
        self.regenerationCalories += recette.gainDurable[2]
        self.nbCarteParTour += recette.gainDurable[3]
        if recette.gainDurable[4] != 0: #test si il y a un multiplicateur de dégat
            self.attaque = self.attaque * recette.gainDurable[4]

        #instant
        #si des dégat doivent être infligé au glouton adverse alors lève une exception avec en message le nombre de dégat à infligé
        self.PV += recette.gainInstant[1]
        if recette.gainInstant[2] != 0:
            #doit afficher la vue pioche avec comme nombre de carte recette.gainInstant
            return -recette.gainInstant[2]
        if recette.gainInstant[0] != 0:
            return recette.gainInstant[0]


    #la fonction nouveau tour ajoute les ingredients calories et pv en fonction des statistiques du glouton
    def nouveauTour(self):
        self.PV += self.regenerationPV
        self.calories += self.regenerationCalories

        self.ingredient[0] += self.gainFarm(0) #viande
        self.ingredient[1] += self.gainFarm(1) #patate
        self.ingredient[2] += self.gainFarm(2) #fromage
        self.ingredient[3] += self.gainFarm(3) #pâtes

    #la fonction gainFarm renvoie le nombre d'ingrédients que produit la farm avec l'index passé en paramètre
    def gainFarm(self,indexFarm) -> int:
        return{
            1:1,
            2:2,
            3:4,
            4:6
        }.get(self.farm[indexFarm],0)
