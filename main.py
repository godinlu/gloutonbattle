
import pygame
from pygame.locals import *
from fonctionmain import *
import controler
from glouton import Glouton

dimension = 1

pygame.init()
file = 'music/Chameleon.mp3'
pygame.mixer.init()
pygame.mixer.music.load(file)
pygame.mixer.music.play(-1)
#Ouverture de la fenêtre Pygame

fenetre = pygame.display.set_mode((int(1900*dimension), int(1080*dimension)),RESIZABLE)

#cette fonction renvoie le joueur ennemie du joueur passé en paramètre
def getjoueurEnnemi(joueur) -> Glouton:
	if joueur == gloutonJoueur1:
		return gloutonJoueur2
	else:
		return gloutonJoueur1

etat = "accueil"


while etat != "quitter":
	etat = accueil(fenetre)
	gloutonJoueur1, gloutonJoueur2 =  controler.initialisation()
	joueurcourant = gloutonJoueur1
	while etat == "credit":
		etat =  credit(fenetre)
	while etat == "regle":
		etat = regle(fenetre)
	while etat == "jeu":
		if etat != "quitter":
			etat = changementJoueur(joueurcourant,fenetre)
		if etat != "quitter":
			etat = pioche(joueurcourant,joueurcourant.nbCarteParTour,fenetre)
		if etat != "quitter":
				etat = action(joueurcourant,getjoueurEnnemi(joueurcourant),fenetre)

		if etat != "quitter":
			if joueurcourant == gloutonJoueur2:
				etat = nuit(fenetre,gloutonJoueur1,gloutonJoueur2)
				#début de la nuit les gloutons s'inflige leurs dégats respectif
				gloutonJoueur1.PV -= gloutonJoueur2.attaque
				gloutonJoueur2.PV -= gloutonJoueur1.attaque
				if gloutonJoueur1.PV <= 0 and gloutonJoueur2.PV <= 0:
					print("égalité")
					etat = fin(fenetre,0)
				elif gloutonJoueur1.PV <= 0:
					print("victoire du joueur 2")
					etat = fin(fenetre,"j2")
				elif gloutonJoueur2.PV <= 0:
					etat = fin(fenetre,"j1")
					print("victoire du joueur 1")
				gloutonJoueur1.nouveauTour()
				gloutonJoueur2.nouveauTour()
			joueurcourant = getjoueurEnnemi(joueurcourant)
