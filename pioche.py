from aliment import Aliment
from recette import Recette
from farm import Farm
import random

class Pioche:
    def __init__(self):
        self.cartes_aliment = []
        self.cartes_recette = []
        self.cartes_farmes = []
        
#la methode addCartes ajoute une carte dans la bonne pioche en fonction de son type 
    def addCartes(self,carte):
        if (isinstance(carte,Recette)):
            self.cartes_recette.append(carte)
        elif (isinstance(carte,Aliment)):
            self.cartes_aliment.append(carte)
        else:
            self.cartes_farmes.append(carte)

    def piocheCarteRecette(self) -> Recette:
        recette = self.cartes_recette[0]
        self.cartes_recette.pop(0)
        return recette

    def piocheCarteAliment(self) -> Aliment:
        aliment = self.cartes_aliment[0]
        self.cartes_aliment.pop(0)
        return aliment

    def piocheCarteFarme(self) -> Farm:
        farm = self.cartes_farmes[0]
        self.cartes_farmes.pop(0)
        return farm

    def shuffle(self):
        random.shuffle(self.cartes_aliment)
        random.shuffle(self.cartes_farmes) 
        random.shuffle(self.cartes_recette) 
    






