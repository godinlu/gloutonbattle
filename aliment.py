class Aliment:
    #apportIngredient est ce que la carte apporte comme aliment de base elle est de la forme [nbViande,nbPatates,nbFromages,nbPates]
    def __init__(self, a_name,a_apportCalories, a_apportIngredient):
        self.name = a_name
        self.apportCalories = a_apportCalories
        self.apportIngredient = a_apportIngredient

