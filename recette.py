class Recette:
    #(self,int a_coutCalorie,array a_ingredients,array a_gainDurable,array a_gainInstant)

    #a_ingredients est de la forme [nbViande,nbPatates,nbFromages,nbPates] 
    #a_gainDurable est de la forme [gainAttaque, gainPVParTour, gainCaloriesParTour, gainCartesParTour, multiplicateurAttaque]
    #a_gainInstant est de la forme [dégatInfligé, gainPv, nbCarteAPiocher]
    #si une valeur est a zéro cela veut dire quelle ne doit pas être pris en compte
    def __init__(self,a_name,a_coutCalorie, a_ingredients, a_gainDurable, a_gainInstant):
        self.name = a_name
        self.coutCalorie = a_coutCalorie
        self.ingredients = a_ingredients
        self.gainDurable = a_gainDurable
        self.gainInstant = a_gainInstant

    def getNbViande(self) -> int:
        return self.ingredients[0]
    
    def getNbPatate(self)-> int:
        return self.ingredients[1]

    def getNbFromage(self) -> int:
        return self.ingredients[2]
    
    def getNbPates(self)-> int:
        return self.ingredients[3]


    def getGainAttaque(self) -> int:
        return self.gainDurable[0]

    def getGainPVParTour(self) -> int:
        return self.gainDurable[1]

    def getGainCaloriesParTour(self) -> int:
        return self.gainDurable[2]

    def getGainCartesParTour(self) -> int:
        return self.gainDurable[3]
    
    def getMultiplicateurAttaque(self) -> int:
        return self.gainDurable[4]
    

    def getDegatInflige(self) -> int:
        return self.gainInstant[0]

    def getGainPV(self) -> int:
        return self.gainInstant[1]
    
    def getNbCartesAPiocher(self) -> int:
        return self.gainInstant[2]
    




    