
from glouton import Glouton
from recette import Recette

glouton = Glouton("lucos")

glouton.initialisationPioche()

glouton.piocheDesCartesInitial()

for carte in glouton.main:
    print(carte.name)


glouton.calories = 12


index = 3
if glouton.peutPoserCarte(index) == 0:
    print("la carte "+ str(glouton.main[index].name) + " a bien été joué")
    try:
        glouton.joueCarte(index)
    except Exception as e:
        print(e)
else:
    print("la carte "+ str(glouton.main[index].name) + "ne peut pas être joué")

print("nb de viande :" + str(glouton.ingredient[0]))
print("nb de patate :" + str(glouton.ingredient[1]))
print("nb de fromage :" + str(glouton.ingredient[2]))
print("nb de pates :" + str(glouton.ingredient[3]))
print("nb de calories :" + str(glouton.calories))

glouton.nouveauTour()

print("nb de viande :" + str(glouton.ingredient[0]))
print("nb de patate :" + str(glouton.ingredient[1]))
print("nb de fromage :" + str(glouton.ingredient[2]))
print("nb de pates :" + str(glouton.ingredient[3]))
print("nb de calories :" + str(glouton.calories))

