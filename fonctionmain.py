import string
import pygame
from pygame.locals import *
from animation import *
from glouton import Glouton



dimension = 1


def bouton():
	bouton = pygame.mixer.Sound('music/bouton.wav')
	bouton.play(0)

def affichageCartes(liste,fenetre):
	x = 15*dimension
	for carte in liste:
		carte = pygame.image.load("img/Carte-"+carte.name+".png").convert_alpha()
		carte = pygame.transform.scale(carte,(int(222*dimension),int(300*dimension)))
		fenetre.blit(carte, (x,660*dimension))
		x += 235*dimension

def clickablePioche(event,joueur,nbCarteAPioche)-> int:
		pioche = pygame.mixer.Sound('music/card.wav')

		if event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=225*dimension and event.pos[0]<=475*dimension and event.pos[1]>=180*dimension and event.pos[1]<=590*dimension:
			pioche.play(0)
			joueur.piocheCarteFarm()

			return nbCarteAPioche - 1
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=771*dimension and event.pos[0]<=1021*dimension and event.pos[1]>=183*dimension and event.pos[1]<=593*dimension:
			pioche.play(0)
			joueur.piocheCarteAliment()

			return nbCarteAPioche - 1
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=1317*dimension and event.pos[0]<=1567*dimension and event.pos[1]>=183*dimension and event.pos[1]<=593*dimension:
			pioche.play(0)
			joueur.piocheCarteRecette()

			return nbCarteAPioche - 1

		return nbCarteAPioche


def clickableCarteZoom(event) -> bool:
	if(event.type == MOUSEBUTTONDOWN and event.button == 1):
		if (event.pos[0] >= 1350*dimension and event.pos[0] <= 1850*dimension
		and event.pos[1] >= 40*dimension and event.pos[1] <= 640*dimension):
			return True
		else:
			return False






#clickableCarte prend en paramètre un event et la taille de la main d'un glouton puis renvoie l'index de la carte que le joueur a clické
#renvoie rien si aucune carte n'a été cliqué
def clickableCarte(event,taille):
		if event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=15*dimension and event.pos[0]<=233*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 1:
				return 0
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=250*dimension and event.pos[0]<=468*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 2:
				return 1
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=485*dimension and event.pos[0]<=703*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 3:
				return 2
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=720*dimension and event.pos[0]<=938*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 4:
				return 3
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=955*dimension and event.pos[0]<=1173*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 5:
				return 4
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=1190*dimension and event.pos[0]<=1408*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 6:
				return 5
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=1425*dimension and event.pos[0]<=1643*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 7:
				return 6
		elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=1660*dimension and event.pos[0]<=1878*dimension and event.pos[1]>=664*dimension and event.pos[1]<=957*dimension:
			if  taille >= 8:
				return 7


def pioche(joueur,nbCarteAPiocher,fenetre)->string:
	pioche = 1
	if len(joueur.main)==8:
		pioche=0
	carteRestante = nbCarteAPiocher
	fond = pygame.image.load("img/pioche.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	pygame.display.update()
	while pioche:
		myfont = pygame.font.SysFont('Comic Sans MS', int(80*dimension))
		textsurface = myfont.render(str(carteRestante), True, (120, 159, 44))
		texthide = myfont.render(str(carteRestante+1), True, (159, 62, 44))
		fenetre.blit(texthide,(905*dimension,25*dimension))
		fenetre.blit(textsurface,(905*dimension,25*dimension))

		for event in pygame.event.get():
			if event.type == QUIT:
				return "quitter"
			carteRestante = clickablePioche(event,joueur,carteRestante)
			clickableCarte(event,len(joueur.main))
			affichageCartes(joueur.main,fenetre)
			pygame.display.update()

		if carteRestante == 0 or len(joueur.main)==8:
			pioche=0


	return "jeu"

def accueil(fenetre)->string:

	a = 1

	fond = pygame.image.load("img/Acceuil.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	pygame.display.update()
	while a:
		for event in pygame.event.get():	#Attente des événements
			if event.type == QUIT:
				return "quitter"
			elif event.type == KEYDOWN:
				if event.key == K_F1:
					return "jeu"
				if event.key == K_F2:
					return "regle"
				if event.key == K_F3:
					return "credit"

def credit(fenetre)->string:
	a = 1
	fond = pygame.image.load("img/Credit.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	pygame.display.update()
	a = 1
	while a:
		for event in pygame.event.get():	#Attente des événements
			if event.type == QUIT:
				return "quitter"
			elif event.type == KEYDOWN:
				if event.key == K_SPACE:
					return "accueil"

def regle(fenetre)->string:

	a = 1

	fond = pygame.image.load("img/RuleP1.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	pygame.display.update()
	while a:
		for event in pygame.event.get():	#Attente des événements
			if event.type == QUIT:
				return "quitter"
			elif event.type == KEYDOWN:
				if event.key == K_SPACE:
					a = 0
	fond = pygame.image.load("img/RuleP2.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	pygame.display.update()
	a = 1
	while a:
		for event in pygame.event.get():	#Attente des événements
			if event.type == QUIT:
				return "quitter"
			elif event.type == KEYDOWN:
				if event.key == K_SPACE:
					return "accueil"




#action prend en paramètre le joueur courant et affiche la vue principal du jeu renvoie 0 si la fenêtre est quitté
def action(joueur,joueurEnnemi,fenetre)->string:
	carteSelection = -2  #carte selection sert de repert si elle est a -2 alors il faut actualisé la page
	#si il est a -1 alors il ne faut rien faire et si il est supérieur a -1 alors il faut traiter carteSelection comme l'id de la carte
	a = 1
	while a:
		pygame.display.update()
		for event in pygame.event.get():
			if event.type == QUIT: #gère la fermeture de la page
				a = 0
				return "quitter"
			elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=107*dimension and event.pos[0]<=392*dimension and event.pos[1]>=500*dimension and event.pos[1]<=614*dimension:
				a =0
				bouton()
				return "jeu"

			if carteSelection >=0: #si une carte a été zoomé alors on attend un click sur la carte

				if clickableCarteZoom(event): #si un click sur la carte zoomé est détecter on entame les procédure de jouer la carte
					errorMsg = joueur.peutPoserCarte(carteSelection)
					if  errorMsg == 0: #errorMsg est a 0 donc la carte peut être joué
						action = joueur.joueCarte(carteSelection)
						eat = pygame.mixer.Sound('music/eat.wav')
						eat.play(0)
						if  action !=None:
							if action < 0: #si action est < 0 alors le glouton doit piocher -action cartes
								a = pioche(joueur,-action,fenetre)
							elif action > 0: #si action est > 0 alors le glouton ennemi doit perdre action PV
								joueurEnnemi.PV -= action
					else:
						for i in range(2):
							affichageVueAction(joueur,joueurEnnemi,fenetre)
							pygame.time.wait(250)
							ressourceInsuffisante(fenetre,errorMsg)
							pygame.time.wait(250)
					carteSelection = -2
				elif(clickableCarteZoom(event) == False): #si un click a été fait en dehors de la cartes il faut enlevé la carte zoomé
					carteSelection = -2

			elif carteSelection == -2:
				carteSelection = -1
				affichageVueAction(joueur,joueurEnnemi,fenetre)

			idCarte = clickableCarte(event,len(joueur.main))  #idCarte prend la valeur de l'id de la carte dans la main de l'utilisateur
			if idCarte != None:	#si une carte a été cliqué elle est zoomé et carteSelection prend la valeur de son Id
				carteSelection = idCarte
				zoom(fenetre,joueur.main[idCarte])

	return "jeu"
#affiche la vue action avec les cartes et les statistiques
def affichageVueAction(joueur,joueurEnnemi,fenetre):

	fond = pygame.image.load("img/interfacejeu.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	affichageCartes(joueur.main,fenetre)
	afficheStatistiques(joueur,joueurEnnemi,fenetre)
	myfont = pygame.font.SysFont('Comic Sans MS', int(80*dimension))
	if joueur.name == "j1":
		textsurface = myfont.render("Joueur 1", True, (120, 159, 44))
	elif joueur.name == "j2":
		textsurface = myfont.render("Joueur 2", True, (120, 159, 44))
	fenetre.blit(textsurface,(800*dimension,550*dimension))
	myfont = pygame.font.SysFont('Comic Sans MS', int(50*dimension))
	if joueur.name == "j1":
		textj2 = myfont.render("Joueur 2", True, (120, 159, 44))
	elif joueur.name == "j2":
		textj2 = myfont.render("Joueur 1", True, (120, 159, 44))
	fenetre.blit(textj2,(1725*dimension,400*dimension))
	pygame.display.update()
	pygame.display.flip()

#affiche toutes les statistiques à l'écran
def afficheStatistiques(joueur,joueurEnnemi,fenetre):
	myfont = pygame.font.SysFont('Comic Sans MS', int(70*dimension))
	smallFont = pygame.font.SysFont('Comic Sans MS', int(40*dimension))
	viande = myfont.render(str(joueur.ingredient[0]), True, (255, 255, 255))
	patate = myfont.render(str(joueur.ingredient[1]), True, (255, 255, 255))
	fromage = myfont.render(str(joueur.ingredient[2]), True, (255, 255, 255))
	pates = myfont.render(str(joueur.ingredient[3]), True, (255, 255, 255))
	carteParTour = myfont.render(str(joueur.nbCarteParTour), True, (255, 255, 255))
	regenerationPV = myfont.render(str(joueur.regenerationPV), True, (255, 255, 255))
	regenerationCalories = myfont.render(str(joueur.regenerationCalories), True, (255, 255, 255))
	calories = myfont.render(str(joueur.calories), True, (255, 255, 255))
	attaque = myfont.render(str(joueur.attaque), True, (255, 255, 255))
	PV = myfont.render(str(joueur.PV), True, (255, 255, 255))
	attaqueEnnemie = myfont.render(str(joueurEnnemi.attaque), True, (255, 255, 255))
	PVEnnemie = myfont.render(str(joueurEnnemi.PV), True, (255, 255, 255))

	viandeTour = smallFont.render(str(joueur.gainFarm(0)), True, (255, 255, 255))
	patateTour = smallFont.render(str(joueur.gainFarm(1)), True, (255, 255, 255))
	fromageTour = smallFont.render(str(joueur.gainFarm(2)), True, (255, 255, 255))
	pateTour = smallFont.render(str(joueur.gainFarm(3)), True, (255, 255, 255))


	fenetre.blit(fromage, (10*dimension,10*dimension))
	fenetre.blit(patate, (10*dimension,70*dimension))
	fenetre.blit(pates, (148*dimension,10*dimension))
	fenetre.blit(viande, (148*dimension,70*dimension))

	fenetre.blit(carteParTour, (575*dimension,37*dimension))
	fenetre.blit(regenerationPV, (997*dimension,37*dimension))
	fenetre.blit(regenerationCalories, (1375*dimension,37*dimension))
	fenetre.blit(calories, (1600*dimension,45*dimension))
	fenetre.blit(attaque, (650*dimension,430*dimension))
	fenetre.blit(PV, (1210*dimension,430*dimension))
	fenetre.blit(attaqueEnnemie, (1663*dimension,336*dimension))
	fenetre.blit(PVEnnemie, (1790*dimension,336*dimension))

	fenetre.blit(pateTour, (67*dimension,270*dimension))
	fenetre.blit(viandeTour, (67*dimension,431*dimension))
	fenetre.blit(patateTour, (320*dimension,270*dimension))
	fenetre.blit(fromageTour, (320*dimension,431*dimension))

#prend en paramètre
def zoom(fenetre,carte):
	carte = pygame.image.load("img/Carte-"+carte.name+".png").convert_alpha()
	carte = pygame.transform.scale(carte,(int(500*dimension),int(600*dimension)))
	fenetre.blit(carte, (1350*dimension,40*dimension))


def changementJoueur(joueur,fenetre) -> string:
	fond = pygame.image.load("img/changementJoueur.png").convert()
	fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
	fenetre.blit(fond, (0,0))
	pygame.display.flip()
	myfont = pygame.font.SysFont('Comic Sans MS', int(80*dimension))
	if joueur.name == "j1":
		textsurface = myfont.render("1", True, (120, 159, 44))
	elif joueur.name == "j2":
		textsurface = myfont.render("2", True, (120, 159, 44))
	fenetre.blit(textsurface,(780*dimension,350*dimension))
	pygame.display.update()
	a=1
	while a:
		for event in pygame.event.get():
			if event.type == QUIT:
				a = 0
				return "quitter"

			elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=752*dimension and event.pos[0]<=996*dimension and event.pos[1]>=512*dimension and event.pos[1]<=675*dimension:
				bouton()
				return "jeu"



def nuit(fenetre,joueur1,joueur2) -> string:
	for i in range(50):
		if i % 2 == 0:
			img = "Bataille/Bataille-"+str(int(i/2+1))+".png"
		fond = pygame.image.load(img).convert()
		fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
		fenetre.blit(fond, (0,0))
		pygame.display.update()
	sword = pygame.mixer.Sound('music/sword.wav')
	sword.play(0)
	for i in range(30):
		if i <= 9:
			if i % 2 == 0:
				img = "Bataille/Bataille-26.png"
			else:
				img = "Bataille/Bataille-27.png"
		elif i> 9 and i <=19:
			if i % 2 == 0:
				img = "Bataille/Bataille-28.png"
			else:
				img = "Bataille/Bataille-29.png"
		elif i> 19 and i <=29:
			if i % 2 == 0:
				img = "Bataille/Bataille-30.png"
			else:
				img = "Bataille/Bataille-31.png"
		fond = pygame.image.load(img).convert()
		fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
		fenetre.blit(fond, (0,0))
		pygame.display.update()
	#début de la nuit les glouto

	for i in [32,33,34,35,36,37]:

		for t in range(4):
			img = "Bataille/Bataille-"+str(int(i))+".png"
			fond = pygame.image.load(img).convert()
			fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
			fenetre.blit(fond, (0,0))
			pygame.display.update()
	fintour = 1

	myfont = pygame.font.SysFont('Comic Sans MS', int(60*dimension))

	attaqueJ1 = myfont.render(str(joueur1.attaque), True, (255, 0, 0))
	attaqueJ2 = myfont.render(str(joueur2.attaque), True, (255, 0, 0))


	while fintour:
		img = "Bataille/Bataille-38.png"
		fond = pygame.image.load(img).convert()
		fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
		fenetre.blit(fond, (0,0))
		fenetre.blit(attaqueJ2, (1350*dimension,838*dimension))
		fenetre.blit(attaqueJ1, (1350*dimension,910*dimension))
		pygame.display.update()
		for event in pygame.event.get():
			if event.type == QUIT:
				fintour = 0
				return "quitter"
			elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=0 :
				fintour = 0
				return "jeu"

def fin(fenetre,joueur)->string:
	victoire = pygame.mixer.Sound('music/victoire.wav')
	victoire.play(0)
	for i in [39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57]:
		for t in range(4):
			img = "Bataille/Bataille-"+str(i)+".png"
			fond = pygame.image.load(img).convert()
			fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
			fenetre.blit(fond, (0,0))
			pygame.display.update()
	fin = 1
	while fin:
		img = "Bataille/Bataille-58.png"
		fond = pygame.image.load(img).convert()
		fond = pygame.transform.scale(fond,(int(1920*dimension),int(1080*dimension)))
		fenetre.blit(fond, (0,0))
		myfont = pygame.font.SysFont('Comic Sans MS', int(80*dimension))
		if joueur == "j1":
			textsurface = myfont.render("1", True, (21, 116, 21))
			fenetre.blit(textsurface,(825*dimension,815*dimension))
		elif joueur == "j2":
			textsurface = myfont.render("2", True, (21, 116, 21))
			fenetre.blit(textsurface,(825*dimension,815*dimension))
		else:
			textsurface = myfont.render("1&2", True, (21, 116, 21))
			fenetre.blit(textsurface,(780*dimension,815*dimension))

		pygame.display.update()
		for event in pygame.event.get():
			if event.type == QUIT:
				fintour = 0
				return "quitter"
			elif event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0]>=0 :
				fin = 0
				return "accueil"
