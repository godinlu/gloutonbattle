from aliment import Aliment
from recette import Recette
from farm import Farm

    #a_ingredients est de la forme [nbViande,nbPatates,nbFromages,nbPates] 
    #a_gainDurable est de la forme [gainAttaque, gainPVParTour, gainCaloriesParTour, gainCartesParTour, multiplicateurAttaque]
    #a_gainInstant est de la forme [dégatInfligé, gainPv, nbCarteAPiocher]

#carte recette
lasagne = Recette(
    "lasagne",
    2,
    [2,0,2,4],
    [0,0,0,1,0],
    [0,5,0]
)
hamburger = Recette(
    "hamburger",
    5,
    [3,2,2,0],
    [1,0,0,1,0],
    [3,0,0]
)
hachiParmentier = Recette(
    "hachi-parmentier",
    7,
    [2,5,0,0],
    [0,0,0,2,0],
    [0,4,0]
)
tartiflette = Recette(
    "tartiflette",
    6,
    [0,5,3,0],
    [2,1,0,0,0],
    [0,0,0]
)
frites = Recette(
    "frites",
    3,
    [0,3,0,0],
    [0,0,1,0,0],
    [0,0,0]
)
tacos = Recette(
    "tacos",
    10,
    [5,4,2,0],
    [0,0,0,0,2],
    [0,6,0]
)
steak = Recette(
    "steak",
    3,
    [2,0,0,0],
    [0,2,0,0,0],
    [0,0,0]
)
patesCarbo = Recette(
    "carbonara",
    4,
    [1,0,1,3],
    [1,0,1,0,0],
    [0,0,0]
)
croziflette = Recette(
    "croziflette",
    6,
    [1,0,2,4],
    [2,0,0,1,0],
    [0,0,0]
)
fondu = Recette(
    "fondu",
    7,
    [0,0,5,0],
    [3,0,0,0,0],
    [2,0,0]
)
pouletPatate = Recette(
    "poulet-patate",
    3,
    [1,2,0,0],
    [1,0,0,0,0],
    [0,0,1]
)
raclette = Recette(
    "raclette",
    12,
    [4,4,4,0],
    [2,1,1,1,0],
    [0,0,0]
)
pateBolo = Recette(
    "bolognaise",
    4,
    [1,0,1,2],
    [2,0,0,0,0],
    [1,0,0]
)
pateAuBeurre = Recette(
    "pate-au-beurre",
    7,
    [0,0,1,7],
    [0,2,0,0,0],
    [0,6,0]
)
#carte farm 
boucherie = Farm(
    "boucherie",
    5,
    [1,0,0,0]
)
champDePatate = Farm(
    "champ-de-patate",
    3,
    [0,1,0,0]
)
fromagerie = Farm(
    "fromagerie",
    4,
    [0,0,1,0]
)
pateries = Farm(
    "paterie",
    3,
    [0,0,0,1]
)
#carte aliment
tasDeViande = Aliment(
    "viande",
    1,
    [1,0,0,0]
)
sacDePatate = Aliment(
    "patate",
    1,
    [0,2,0,0]
)
meuleDeFromage = Aliment(
    "fromage",
    1,
    [0,0,1,0]
)
sachetDePates = Aliment(
    "pate",
    1,
    [0,0,0,2]
)


cartes_recette = [lasagne,hamburger,hachiParmentier,tartiflette,frites,tacos,steak,patesCarbo,croziflette,fondu,pouletPatate,raclette,pateBolo,pateAuBeurre]
nb_cartes_recette = [2,2,2,3,7,2,4,4,3,4,6,2,4,4] #désigne le nb de cartes qui sera dans la pioche en fonction de l'index

cartes_farm = [boucherie,champDePatate,fromagerie,pateries]
nb_cartes_farm = [4,4,4,4]  #désigne le nb de cartes qui sera dans la pioche en fonction de l'index

cartes_aliment = [tasDeViande,sacDePatate,meuleDeFromage,sachetDePates]
nb_cartes_aliment = [7,7,7,7]  #désigne le nb de cartes qui sera dans la pioche en fonction de l'index


